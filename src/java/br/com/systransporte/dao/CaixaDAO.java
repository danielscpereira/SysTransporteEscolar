/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.systransporte.dao;

import br.com.systransporte.model.Caixa;
import br.com.systransporte.model.Credito;
import br.com.systransporte.model.Debito;
import br.com.systransporte.model.TipoLancamento;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Rick Novo
 */
public class CaixaDAO extends DAO<Caixa> {

    public CaixaDAO() {
        super(Caixa.class);
    }

    public List<TipoLancamento> findAllLancamentos() {

        this.beginTransaction();
        
        List<TipoLancamento> debitos = em.createQuery("select d from Debito d ").getResultList();

        List<TipoLancamento> creditos = em.createQuery("select c from Credito c ").getResultList();

        List<TipoLancamento> lancamentos = new ArrayList<>();
        lancamentos.addAll(creditos);
        lancamentos.addAll(debitos);
       lancamentos.sort((p1, p2) -> p1.getDataLancamento().compareTo(p2.getDataLancamento()));
        
        this.closeTransaction();
        
        return lancamentos;
        
    }

}
