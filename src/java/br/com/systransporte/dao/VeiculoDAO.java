/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.systransporte.dao;

import br.com.systransporte.model.Veiculo;
import java.util.List;
import javax.persistence.Query;

/**
 *
 * @author Rick Novo
 */
public class VeiculoDAO extends DAO<Veiculo> {

    public VeiculoDAO() {
        super(Veiculo.class);
    }

    public List<Veiculo> findAllAtivos() {

        Query query = em.createQuery("from Veiculo v where v.ativo = :ativo");
        query.setParameter("ativo", true);

        return query.getResultList();
    }
    
    public Veiculo findVeiculoEvento(String marca, String modelo, String cor,String Placa){
        Query query = em.createQuery("from Veiculo v where v.ativo = :ativo");
        query.setParameter("ativo", true);
       return null;
    }

}
