/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.systransporte.dao;

import br.com.systransporte.model.Turno;
import java.util.List;

/**
 *
 * @author Administrador
 */
public class TurnoDAO extends DAO<Turno>{

    public TurnoDAO() {
        super(Turno.class);
    }

    public List<Turno> findTurnos() {
       return em.createQuery("t.turno from Turno t").getResultList();
    }
    
}
