/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.systransporte.dao;

import br.com.systransporte.model.Usuario;
import java.util.List;
import javax.persistence.Query;

/**
 *
 * @author Rick Novo
 */
public class UsuarioDAO extends DAO<Usuario> {

    public UsuarioDAO() {
        super(Usuario.class);
    }

    public List<Usuario> findAllAtivos() {
         Query query = em.createQuery("from Usuario u where u.ativo = :ativo");
        query.setParameter("ativo", true);

        return query.getResultList();
    }

    public Usuario findByLogin(String login) {
        Usuario usuario = null;
        try {
            usuario = em.createQuery("from Usuario u where u.usuario = '" + login + "'", Usuario.class).getSingleResult();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return usuario;

    }

}
