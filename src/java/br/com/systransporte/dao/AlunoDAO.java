/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.systransporte.dao;

import br.com.systransporte.model.Aluno;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Query;

/**
 *
 * @author Rick Novo
 */
public class AlunoDAO extends DAO<Aluno> {

    public AlunoDAO() {
        super(Aluno.class);
    }

    @Override
    public Aluno update(Aluno aluno) {

        em.merge(aluno);
        em.merge(aluno.getEndereco());

        return aluno;
    }

    public List<Aluno> findAllAtivos() {
        Query query = em.createQuery("from Aluno a where a.ativo = :ativo");
        query.setParameter("ativo", true);

        return query.getResultList();
    }

    public List<Aluno> findByNome(String nome) {
        Query query = em.createQuery("from Aluno a join Turno t on a.turno_id = t.id where a.ativo = :ativo and a.nome like '" + nome + "%'");
        query.setParameter("ativo", true);
        return query.getResultList();
    }

    public List<Aluno> find(String nome, Date dataInicio, Date dataFim) {

        List<Aluno> listaAluno = new ArrayList<>();

        try {
            this.beginTransaction();

            StringBuilder sb = new StringBuilder();
            sb.append("from Aluno a where 1=1 and a.ativo = :ativo");

            if (nome != null && !nome.equals("")) {
                sb.append(" and a.nome like :nome");

            }

            if (dataInicio != null) {
                sb.append(" and a.dataVencimento >= :dataInicio and a.dataVencimento <= :dataFim ");

            }

            Query query = em.createQuery(sb.toString());
            

            if (nome != null && !nome.equals("")) {
                query.setParameter("nome", "%" + nome + "%");
            }
            
            if (dataInicio != null) {
                query.setParameter("dataInicio", dataInicio);
                query.setParameter("dataFim", dataFim);
            }

            query.setParameter("ativo", true);

            listaAluno = query.getResultList();
            this.closeTransaction();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return listaAluno;

    }
}
