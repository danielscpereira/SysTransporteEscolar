/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.systransporte.dao;

import br.com.systransporte.model.Debito;

/**
 *
 * @author Rick Novo
 */
public class DebitoDAO extends DAO<Debito> {

    public DebitoDAO() {
        super(Debito.class);

    }

}
