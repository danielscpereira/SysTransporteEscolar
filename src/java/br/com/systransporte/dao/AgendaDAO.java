/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.systransporte.dao;

import br.com.systransporte.model.Agenda;
import java.util.List;
import javax.persistence.Query;

/**
 *
 * @author Rick Novo
 */
public class AgendaDAO extends DAO<Agenda> {

    public AgendaDAO() {
        super(Agenda.class);
    }

    public List<Agenda> findAllAtivos() {
        Query query = em.createQuery("from Agenda a where a.ativo = :ativo");
        query.setParameter("ativo", true);

        return query.getResultList();
    }

    @Override
    public Agenda update(Agenda agenda) {

        em.merge(agenda);
        em.merge(agenda.getVeiculo());

        return agenda;
    }

}
