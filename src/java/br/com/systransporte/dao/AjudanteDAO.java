/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.systransporte.dao;

import br.com.systransporte.model.Ajudante;
import java.util.List;
import javax.persistence.Query;

/**
 *
 * @author Rick Novo
 */

public class AjudanteDAO extends DAO<Ajudante>{

    public AjudanteDAO() {
        super(Ajudante.class);
    }
    
    
     @Override
    public Ajudante update(Ajudante ajudante) {
       
        em.merge(ajudante);
        em.merge(ajudante.getEndereco());
        
        return ajudante;
    }
    
    
     public List<Ajudante> findAllAtivos() {
       Query query = em.createQuery("from Ajudante a where a.ativo = :ativo");
        query.setParameter("ativo", true);

        return query.getResultList();
    }
    
    
    
}
