/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.systransporte.dao;

import br.com.systransporte.model.Motorista;
import java.util.List;
import javax.persistence.Query;

/**
 *
 * @author Rick Novo
 */
public class MotoristaDAO extends DAO<Motorista> {
    
    public MotoristaDAO() {
        super(Motorista.class);
    }
    
    public void delete(Motorista motorista) {
        this.beginTransaction();
        motorista.setAtivo(false);
        this.update(motorista);
        this.commit();
        
    }
    
    @Override
    public Motorista update(Motorista motorista) {
        
        em.merge(motorista);
        em.merge(motorista.getEndereco());
        
        return motorista;
    }
    
    public List<Motorista> findAllAtivos() {
        Query query = em.createQuery("from Motorista m where m.ativo = :ativo");
        query.setParameter("ativo", true);
        
        return query.getResultList();
    }
    
}
