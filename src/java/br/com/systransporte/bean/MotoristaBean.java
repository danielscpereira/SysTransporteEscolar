/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.systransporte.bean;

import br.com.systransporte.dao.MotoristaDAO;
import br.com.systransporte.model.Motorista;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Rick Novo
 */
@ManagedBean
@ViewScoped
public class MotoristaBean extends Bean {

    private Motorista motorista = new Motorista();
    private final MotoristaDAO dao = new MotoristaDAO();

    /**
     * Creates a new instance of MotoristaBean
     */
    public MotoristaBean() {
    }

    public Motorista getMotorista() {
        return motorista;
    }

    public void setMotorista(Motorista motorista) {
        this.motorista = motorista;
    }

  
    public String salvar() {

        try {
            dao.beginTransaction();
            dao.save(motorista);
            dao.commitAndCloseTransaction();
            addMessageInfo("Motorista Salvo com Sucesso !");
            this.motorista = new Motorista();

        } catch (Exception ex) {
            dao.rollback();
            addMessageErro(ex.getMessage());
        }

        return null;
    }

 public String atualizar() {

        try {
            
            dao.beginTransaction();
            dao.update(motorista);
            dao.commitAndCloseTransaction();
            addMessageInfo("Motorista Atualizado com Sucesso !");
            this.motorista = new Motorista();

        } catch (Exception ex) {
            dao.rollback();
            addMessageErro(ex.getMessage());
        }

        return null;
    }
    public void remover() {

        try {

            this.motorista.setAtivo(false);
            this.dao.beginTransaction();
            this.dao.update(this.motorista);
            this.dao.commitAndCloseTransaction();

            addMessageAviso("Motorista com sucesso !");

        } catch (Exception ex) {
            dao.rollback();
            addMessageErro(ex.getMessage());
        }
    }

    public List<Motorista> getMotoristas() {
        List<Motorista> lista;
        dao.beginTransaction();
        lista = dao.findAllAtivos();
        dao.commitAndCloseTransaction();
        return lista;
    }

}
