/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.systransporte.bean;

import br.com.systransporte.dao.AgendaDAO;
import br.com.systransporte.model.Agenda;
import br.com.systransporte.model.Veiculo;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.event.ValueChangeEvent;

/**
 *
 * @author Rick Novo
 */
@ManagedBean
@SessionScoped
public class AgendaBean extends Bean {

    private final AgendaDAO dao = new AgendaDAO();
    private Agenda agenda = new Agenda();

    public Agenda getAgenda() {
        return agenda;
    }

    public void setAgenda(Agenda agenda) {
        this.agenda = agenda;
    }

    public AgendaBean() {
    }

    public String salvar() {

        try {
            dao.beginTransaction();
            dao.save(agenda);
            dao.commitAndCloseTransaction();
            addMessageInfo("Evento Salvo com Sucesso !");
            this.agenda = new Agenda();

        } catch (Exception ex) {
            dao.rollback();
            addMessageErro(ex.getMessage());
        }

        return null;
    }

    public String atualizar() {

        try {
            dao.beginTransaction();
            dao.update(agenda);
            dao.commitAndCloseTransaction();
            addMessageInfo("Evento Atualizado com Sucesso !");
            this.agenda = new Agenda();

        } catch (Exception ex) {
            dao.rollback();
            addMessageErro(ex.getMessage());
        }

        return null;
    }

    public void remover() {

        try {
            dao.beginTransaction();
            dao.delete(agenda.getId(), Agenda.class);
            dao.commitAndCloseTransaction();
            addMessageAviso("Evento Removido com sucesso !");

        } catch (Exception ex) {
            dao.rollback();
            addMessageErro(ex.getMessage());
        }
    }

    public void cancelar() {
        this.agenda = new Agenda();
    }

    public List<Agenda> getEventos() {
        List<Agenda> lista;
        dao.beginTransaction();
        lista = dao.findAll();
        dao.closeTransaction();
        return lista;
    }

    public void selecionarVeiculo(ValueChangeEvent event) {
        
        
        
        System.out.println("value changed...");
    }

}
