/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.systransporte.bean;

import br.com.systransporte.dao.AlunoDAO;
import br.com.systransporte.model.Aluno;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Rick Novo
 */
@ManagedBean
@ViewScoped
public class AlunoBean extends Bean {

    private Aluno aluno = new Aluno();
    private final AlunoDAO dao = new AlunoDAO();

    public AlunoBean() {
    }

    public Aluno getAluno() {
        return aluno;
    }

    public void setAluno(Aluno aluno) {
        this.aluno = aluno;
    }

    
public String atualizar() {

        try {
            dao.beginTransaction();
            dao.update(aluno);
            dao.commitAndCloseTransaction();
            addMessageInfo("Aluno Atualizado com Sucesso !");
            this.aluno = new Aluno();

        } catch (Exception ex) {
            dao.rollback();
            addMessageErro(ex.getMessage());
        }

        return null;
    }
    
    public String salvar() {

        try {
            dao.beginTransaction();
            dao.save(aluno);
            dao.commitAndCloseTransaction();
            addMessageInfo("Aluno Salvo com Sucesso !");
            this.aluno = new Aluno();

        } catch (Exception ex) {
            dao.rollback();
            addMessageErro(ex.getMessage());
        }

        return null;
    }

    public void remover() {

        try {
            this.aluno.setAtivo(false);
            dao.beginTransaction();
            dao.update(this.aluno);
            dao.commitAndCloseTransaction();
            addMessageAviso("Aluno removido com sucesso !");

        } catch (Exception ex) {
            dao.rollback();
            addMessageErro(ex.getMessage());
        }
    }


    public List<Aluno> getAlunos() {
        List<Aluno> lista;
        dao.beginTransaction();
        lista = dao.findAllAtivos();
        dao.commitAndCloseTransaction();
        return lista;
    }

 

   
}
