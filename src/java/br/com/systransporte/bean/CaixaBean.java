/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.systransporte.bean;

import br.com.systransporte.dao.CaixaDAO;
import br.com.systransporte.dao.LancamentoDAO;
import br.com.systransporte.model.Caixa;
import br.com.systransporte.model.Credito;
import br.com.systransporte.model.Debito;
import br.com.systransporte.model.TipoLancamento;
import java.util.Date;
import java.util.List;
import javax.faces.bean.ManagedBean;

/**
 *
 * @author Rick Novo
 */
@ManagedBean
public class CaixaBean {

    private Caixa caixa = new Caixa();

    public Caixa getCaixa() {
        return caixa;
    }

    public void setCaixa(Caixa caixa) {
        this.caixa = caixa;
    }
    private final LancamentoDAO daoLancamento = new LancamentoDAO();
    private final CaixaDAO dao = new CaixaDAO();
    private String tipo;
    private Date dataLancamento = new Date();
    private String descricao;
    private double valor;
    private TipoLancamento lancamento;
    

    public CaixaBean() {
        List<TipoLancamento> lancamentos = dao.findAllLancamentos();
        this.caixa.setLancamentos(lancamentos);
    }

    public TipoLancamento getLancamento() {
        return lancamento;
    }

    public void setLancamento(TipoLancamento lancamento) {
        this.lancamento = lancamento;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Date getDataLancamento() {
        return dataLancamento;
    }

    public void setDataLancamento(Date dataLancamento) {

        this.dataLancamento = dataLancamento;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public void salvar() {

        if (this.getTipo().equals("Entrada")) {
            this.lancamento = new Credito();
        } else {
            this.lancamento = new Debito();
        }
        this.lancamento.setDataLancamento(this.dataLancamento);
        this.lancamento.setDescricao(this.descricao);
        this.lancamento.setAtivo(true);
        this.lancamento.setValor(this.valor);

        //salvar lancamento 
        try {
            daoLancamento.beginTransaction();
            daoLancamento.save(lancamento);
            daoLancamento.commitAndCloseTransaction();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        //busca o caixa 
        List<TipoLancamento> lancamentos = dao.findAllLancamentos();
        this.caixa.setLancamentos(lancamentos);

    }

}
