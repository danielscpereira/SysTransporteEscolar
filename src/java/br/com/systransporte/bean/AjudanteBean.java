/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.systransporte.bean;

import br.com.systransporte.dao.AjudanteDAO;
import br.com.systransporte.model.Ajudante;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Rick Novo
 */
@ManagedBean
@ViewScoped
public class AjudanteBean extends Bean {

    private Ajudante ajudante = new Ajudante();
    private AjudanteDAO dao = new AjudanteDAO();

    public AjudanteBean() {
    }

    public Ajudante getAjudante() {
        return ajudante;
    }

    public void setAjudante(Ajudante ajudante) {
        this.ajudante = ajudante;
    }



    public String salvar() {

        try {
            dao.beginTransaction();
            dao.save(ajudante);
            dao.commitAndCloseTransaction();
            addMessageInfo("Ajudante Salvo com Sucesso !");
            this.ajudante = new Ajudante();

        } catch (Exception ex) {
            dao.rollback();
            addMessageErro(ex.getMessage());
        }

        return null;
    }

    public String atualizar() {

        try {
            dao.beginTransaction();
            dao.update(ajudante);
            dao.commitAndCloseTransaction();
            addMessageInfo("Ajudante Atualizado com Sucesso !");
            this.ajudante = new Ajudante();

        } catch (Exception ex) {
            dao.rollback();
            addMessageErro(ex.getMessage());
        }

        return null;
    }

    public void remover() {

        try {
            this.ajudante.setAtivo(false);
            dao.beginTransaction();
            dao.update(this.ajudante);
            dao.commitAndCloseTransaction();
            addMessageAviso("Ajudante Removido com sucesso !");

        } catch (Exception ex) {
            dao.rollback();
            addMessageErro(ex.getMessage());
        }
    }

    public List<Ajudante> getAjudantes() {
        List<Ajudante> lista;
        dao.beginTransaction();
        lista = dao.findAllAtivos();
        dao.commitAndCloseTransaction();
        return lista;
    }

}
