/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.systransporte.bean;

import br.com.systransporte.dao.UsuarioDAO;
import br.com.systransporte.model.Usuario;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Rick Novo
 */
@ManagedBean
@ViewScoped
public class UsuarioBean extends Bean {

    UsuarioDAO dao = new UsuarioDAO();
    Usuario usuario = new Usuario();

    /**
     * Creates a new instance of UsuarioBean
     */
    public UsuarioBean() {
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
    
    public String cancelar() {

        this.usuario = new Usuario();
        return null;
    }
    
    public String salvar() {

        try {
            dao.beginTransaction();
            dao.save(usuario);
            dao.commitAndCloseTransaction();
            addMessageInfo("Usuario Salvo com Sucesso !");
            this.usuario = new Usuario();
        } catch (Exception ex) {
            dao.rollback();
            addMessageErro(ex.getMessage());
        }

        return null;
    }
    
    
    public String atualizar() {

        try {
            dao.beginTransaction();
            dao.update(usuario);
            dao.commitAndCloseTransaction();
            addMessageInfo("Usuario Atualizado com Sucesso !");
            this.usuario = new Usuario();

        } catch (Exception ex) {
            dao.rollback();
            addMessageErro(ex.getMessage());
        }

        return null;
    }
    
    
     public void remover() {

        try {

            this.usuario.setAtivo(false);
            this.dao.beginTransaction();
            this.dao.update(this.usuario);
            this.dao.commitAndCloseTransaction();

            addMessageAviso("Usuario com sucesso !");

        } catch (Exception ex) {
            dao.rollback();
            addMessageErro(ex.getMessage());
        }

     }
     
     public void editar(){
         
         
     }
     
     public List<Usuario> getUsuarios() {
        List<Usuario> lista;
        dao.beginTransaction();
         lista = dao.findAllAtivos() ; 
        dao.commitAndCloseTransaction();
        return lista;
    }
}
