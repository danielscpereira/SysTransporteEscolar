/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.systransporte.bean;

import java.io.InputStream;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
 
@ManagedBean
public class downloadManualBean {
     
    private final StreamedContent file;
     
    public downloadManualBean() {        
        InputStream stream = ((ServletContext)FacesContext.getCurrentInstance().getExternalContext().getContext()).getResourceAsStream("/resources/download/Sys Transporte Escolar manual Do Usuário.pdf");
        file = new DefaultStreamedContent(stream, "application/pdf", "Sys Transporte Escolar manual Do Usuário.pdf");
    }
 
    public StreamedContent getFile() {
        return file;
    }
}