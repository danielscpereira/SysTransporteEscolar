/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.systransporte.bean;

import br.com.systransporte.dao.UsuarioDAO;
import br.com.systransporte.model.Usuario;
import java.io.IOException;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Administrador
 */
@ManagedBean
@RequestScoped
public class loginBean extends Bean {

    private final UsuarioDAO dao = new UsuarioDAO();
    private String usuario;
    private String senha;

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String logar() {
        dao.beginTransaction();
        Usuario user = dao.findByLogin(this.usuario);
        dao.closeTransaction();
        if (user != null && user.getSenha().equals(senha)) {

            //colocando o usuario na sessao 
            FacesContext context = FacesContext.getCurrentInstance();
            context.getExternalContext().getSessionMap().put("user", user);

            return "admin/index";

        } else {
            this.addMessageErro("Usuário ou senha invalido !");
        }

        return null;

    }

    public void logout() throws IOException {

        FacesContext context = FacesContext.getCurrentInstance();
        ExternalContext ex = context.getExternalContext();
        ex.invalidateSession();
        ex.redirect("./login.xhtml");

    }

}
