/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.systransporte.bean;

import br.com.systransporte.dao.AlunoDAO;
import br.com.systransporte.model.Aluno;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.faces.bean.ManagedBean;

@ManagedBean
public class PesquisaBean {

    private final AlunoDAO dao = new AlunoDAO();
    private String nome;
    private List<Aluno> listaAluno;
    private Date dataInicio;
    private Date dataFim;

    public List<Aluno> getListaAluno() {
        return listaAluno;
    }

    public void setListaAluno(List<Aluno> listaAluno) {
        this.listaAluno = listaAluno;
    }

    public List<Aluno> getAlunosByNome(String query) {

        List<Aluno> lista;
        dao.beginTransaction();
        lista = dao.findByNome(query);
        dao.commitAndCloseTransaction();
        return lista;
    }

    public void pesquisaAluno() {
        this.listaAluno = dao.find(nome, dataInicio, dataFim);

    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Date getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(Date dataInicio) {
        this.dataInicio = dataInicio;
    }

    public Date getDataFim() {
        return dataFim;
    }

    public void setDataFim(Date dataFim) {

        Calendar c = Calendar.getInstance();

        if (dataFim != null) {
            c.setTime(dataFim);
            c.set(Calendar.HOUR_OF_DAY, 23);
            c.set(Calendar.MINUTE, 59);
            c.set(Calendar.SECOND, 59);
            c.set(Calendar.MILLISECOND, 999);

            this.dataFim = c.getTime();
        } else if (this.dataInicio != null) {
            c.setTime(this.dataInicio);
            c.set(Calendar.HOUR_OF_DAY, 23);
            c.set(Calendar.MINUTE, 59);
            c.set(Calendar.SECOND, 59);
            c.set(Calendar.MILLISECOND, 999);

            this.dataFim = c.getTime();
        } else {
            this.dataFim = dataFim;
        }

    }

}
