/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.systransporte.bean;

import br.com.systransporte.dao.VeiculoDAO;
import br.com.systransporte.model.Veiculo;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Administrador
 */
@ManagedBean
@ViewScoped
public class VeiculoBean extends Bean{

    private final VeiculoDAO dao = new VeiculoDAO();
    private Veiculo veiculo = new Veiculo();

    public Veiculo getVeiculo() {
        return veiculo;
    }

    public void setVeiculo(Veiculo veiculo) {
        this.veiculo = veiculo;
    }
    
    public VeiculoBean() {
    }

      public String salvar() {

        try {
            dao.beginTransaction();
            dao.save(veiculo);
            dao.commitAndCloseTransaction();
            addMessageInfo("Veículo Salvo com Sucesso !");
            this.veiculo = new Veiculo();

        } catch (Exception ex) {
            dao.rollback();
            addMessageErro(ex.getMessage());
        }

        return null;
    }

   public String atualizar() {

        try {
            dao.beginTransaction();
            dao.update(veiculo);
            dao.commitAndCloseTransaction();
            addMessageInfo("Veiculo Atualizado com Sucesso !");
            this.veiculo = new Veiculo();

        } catch (Exception ex) {
            dao.rollback();
            addMessageErro(ex.getMessage());
        }

        return null;
    }

      
    public void remover() {

        try {
            dao.beginTransaction();
            dao.delete(veiculo.getId(), Veiculo.class);
            dao.commitAndCloseTransaction();
            addMessageAviso("Veículo removido com sucesso !");

        } catch (Exception ex) {
            dao.rollback();
            addMessageErro(ex.getMessage());
        }
    }

    public List<Veiculo> getVeiculos() {
        List<Veiculo> lista;
        dao.beginTransaction();
        lista = dao.findAllAtivos();
        dao.closeTransaction();
        return lista;
    }
    
    public Veiculo getVeiculoEvento(String marca, String modelo, String cor,String Placa){
        Veiculo veiculo = new Veiculo();
        dao.beginTransaction();
     //   dao.findVeiculoEvento();
        dao.closeTransaction();
        return veiculo;
    }
    
}
