/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.systransporte.bean;

import br.com.systransporte.dao.TurnoDAO;
import br.com.systransporte.model.Turno;
import java.util.List;
import javax.faces.bean.ManagedBean;

/**
 *
 * @author Rick Novo
 */
@ManagedBean

public class TurnoBean extends Bean{

private Turno turno = new Turno();

    public Turno getTurno() {
        return turno;
    }

    public void setTurno(Turno turno) {
        this.turno = turno;
    }
private final TurnoDAO dao = new TurnoDAO();

    public TurnoBean() {
    }
    
     public List<Turno> getTurnos() {
        List<Turno> lista;
        dao.beginTransaction();
        lista = dao.findAll();
        dao.commitAndCloseTransaction();
        return lista;
    }

    
}
