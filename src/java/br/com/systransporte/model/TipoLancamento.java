package br.com.systransporte.model;

import java.util.Date;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author sala303b
 */
@MappedSuperclass
public abstract class TipoLancamento extends Entidade {

    @Temporal(TemporalType.TIMESTAMP)
    private Date dataLancamento;
    private String descricao;
    private double valor;

    public Date getDataLancamento() {
        return this.dataLancamento;
    }

    public void setDataLancamento(Date dataLancamento) {
        this.dataLancamento = dataLancamento;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public void saveLancamento() {

    }

}
