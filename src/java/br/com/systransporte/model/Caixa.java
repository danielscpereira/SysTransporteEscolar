/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.systransporte.model;


import java.io.Serializable;
import java.util.List;
import javax.persistence.ManyToOne;

/**
 *
 * @author Rick Novo
 */
public class Caixa extends Entidade implements Serializable {

    @ManyToOne(targetEntity = TipoLancamento.class )
    private List<TipoLancamento> lancamentos;
 
    private double saldoAtual;

    public List<TipoLancamento> getLancamentos() {
        return lancamentos;
    }

    public void setLancamentos(List<TipoLancamento> lancamentos) {
        this.lancamentos = lancamentos;
    }

    public void setSaldoAtual(double saldoAtual) {
        this.saldoAtual = saldoAtual;
    }

    public double getSaldoAtual() {

        this.saldoAtual = 0;

        for (TipoLancamento lancamento : this.lancamentos) {
            if (lancamento instanceof Credito) {
                this.saldoAtual += lancamento.getValor();
            } else {
                this.saldoAtual -= lancamento.getValor();
            }
        }

        return this.saldoAtual;

    }

}
