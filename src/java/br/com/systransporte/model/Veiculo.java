/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.systransporte.model;

import java.io.Serializable;
import javax.persistence.Entity;

/**
 *
 * @author Administrador
 */
@Entity
public class Veiculo extends Entidade implements Serializable {

    private int ano;
    private String modelo;
    private String marca;
    private String Disponibilidade;
    private String cor;
    private String placa;
    private String codigoDetran;
    
      public int getAno() {
        return ano;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getCor() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor = cor;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getCodigoDetran() {
        return codigoDetran;
    }

    public void setCodigoDetran(String codigoDetran) {
        this.codigoDetran = codigoDetran;
    }

    public String getDisponibilidade() {
        return Disponibilidade;
    }

    public void setDisponibilidade(String Disponibilidade) {
        this.Disponibilidade = Disponibilidade;
    }

}
