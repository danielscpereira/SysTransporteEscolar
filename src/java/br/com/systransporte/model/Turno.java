/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.systransporte.model;

import java.io.Serializable;
import javax.persistence.Entity;

/**
 *
 * @author Administrador
 */@Entity
public class Turno extends Entidade implements Serializable {
    private String turno;

    public String getTurno() {
        return turno;
    }

    public void setTurno(String turno) {
        this.turno = turno;
    }
}
